#Experiments and calculations to measure the growth rates#

##12.08.14##

Started to measure growth rates for different bacteria: donor, recipient, transconjugant and wt MG1655
 
Experiment runs on 96-well plate, with 200ul medium in each well.
Each strain is grown in the presence of antibiotic (Tet 10ug/ml) and without antibiotic, and with two sets of dilutions from overnight culture: 100x and 1000x. Everything is 3 replicates.
 
Experiment runs for 10h, 37C, with continuous shaking; OD will be measured after every 10min
Raw GrowthRate excel file

Growth rate summary and some simple graphs.

Experiment was in LB media.

##26.08.14##
experiment layout:

|                |   |     | 200x | 200x | 200x | 200x | 1000x | 1000x | 1000x | 1000x |     |     |     |
|----------------|---|-----|------|------|------|------|-------|-------|-------|-------|-----|-----|-----|
|                |   | 1   | 2    | 3    | 4    | 5    | 6     | 7     | 8     | 9     | 10  | 11  | 12  |
|                | a | Neg | Neg  | Neg  | Neg  | Neg  | Neg   | Neg   | Neg   | Neg   | Neg | Neg | Neg |
| Donor          | b | Neg | AB+  | AB+  | AB+  | AB+  | AB+   | AB+   | AB+   | AB+   | Neg | Neg | Neg |
| Recipient      | c | Neg | AB+  | AB+  | AB+  | AB+  | AB+   | AB+   | AB+   | AB+   | Neg | Neg | Neg |
| Transconjugant | d | Neg | AB+  | AB+  | AB+  | AB+  | AB+   | AB+   | AB+   | AB+   | Neg | Neg | Neg |
| Donor          | e | Neg | AB-  | AB-  | AB-  | AB-  | AB-   | AB-   | AB-   | AB-   | Neg | Neg | Neg |
| Recipient      | f | Neg | AB-  | AB-  | AB-  | AB-  | AB-   | AB-   | AB-   | AB-   | Neg | Neg | Neg |
| Transconjugant | g | Neg | AB-  | AB-  | AB-  | AB-  | AB-   | AB-   | AB-   | AB-   | Neg | Neg | Neg |
|                | h | Neg | Neg  | Neg  | Neg  | Neg  | Neg   | Neg   | Neg   | Neg   | Neg | Neg | Neg |


Experiment on 96-well.
The media used was 0.25% CM, 200ul per well. The cells were inoculated from DMSO stocks. Antibiotic used - Tetracycline (10ug/ml). Will measure for 24h; OD will be measured after every 10min.
Next will grow them also on 37C and with kanamycin. 





##27.08.14##

First observation showed that there was no growth in yesterdays experiment and it’s hard to say why.
Made similar experiment, just on 37C to see if the temperature is influencing this. Used again the DSMO stocks that are made from the same overnight culture. The first experiment done with these strains was done from overnight cultures and not from DMSO stocks.
Antibiotic used was tetracycline. 
dilution - 200x -- 1ul of DMSO stock
	  1000x -- 1ul of 5x dilution of DMSO stock

experiment layout:

|                |   |     | 200x | 200x | 200x | 200x | 1000x | 1000x | 1000x | 1000x |     |     |     |
|----------------|---|-----|------|------|------|------|-------|-------|-------|-------|-----|-----|-----|
|                |   | 1   | 2    | 3    | 4    | 5    | 6     | 7     | 8     | 9     | 10  | 11  | 12  |
|                | a | Neg | Neg  | Neg  | Neg  | Neg  | Neg   | Neg   | Neg   | Neg   | Neg | Neg | Neg |
| Donor          | b | Neg | AB+  | AB+  | AB+  | AB+  | AB+   | AB+   | AB+   | AB+   | Neg | Neg | Neg |
| Recipient      | c | Neg | AB+  | AB+  | AB+  | AB+  | AB+   | AB+   | AB+   | AB+   | Neg | Neg | Neg |
| Transconjugant | d | Neg | AB+  | AB+  | AB+  | AB+  | AB+   | AB+   | AB+   | AB+   | Neg | Neg | Neg |
| Donor          | e | Neg | AB-  | AB-  | AB-  | AB-  | AB-   | AB-   | AB-   | AB-   | Neg | Neg | Neg |
| Recipient      | f | Neg | AB-  | AB-  | AB-  | AB-  | AB-   | AB-   | AB-   | AB-   | Neg | Neg | Neg |
| Transconjugant | g | Neg | AB-  | AB-  | AB-  | AB-  | AB-   | AB-   | AB-   | AB-   | Neg | Neg | Neg |
|                | h | Neg | Neg  | Neg  | Neg  | Neg  | Neg   | Neg   | Neg   | Neg   | Neg | Neg | Neg |


The cells were growing at 37C. Now will try with 28C again.

##28.08.14##

The same growth rate experiment as before on 28 degrees. Will see if there were any mistakes done before, because at 37C there was growth, although, with 1000x dilution growth wasn’t as good. OD didn’t climb as 200x dilution. 

There was growth, but not as strong as on 37C. Need to analyze and compare these two. In addition there might be a problem with aeration, I am using 200ul of media per well. Arvi said that he has seen in his experiments that the volume of media influences the wake up of E. coli - probably because of aeration is different. Have to try out the same conditions with just a different amount of growth media and how these influence the growth rate.

##03.09.14##

Made similar experiment on 28C as before, but the media used is LB. The layout, antibiotic (tet), and dilutions are the same as before.
