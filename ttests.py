import pandas as pd
import numpy as np
from itertools import permutations
from itertools import combinations
from scipy import stats

strain_position_on_plate = { 'donorAB':['B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B9'],
                              'donorABminus':['E2', 'E3', 'E4', 'E5', 'E6', 'E7', 'E8', 'E9'],
                             'recipAB':['C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9'], 
                              'recipABminus':['F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'F8', 'F9'],
                              'transcAB':['D2', 'D3', 'D4', 'D5', 'D6', 'D7', 'D8', 'D9'],
                              'transcABminus':['G2', 'G3', 'G4', 'G5', 'G6', 'G7', 'G8', 'G9']}

data28CM = pd.read_csv('/home/voolaid/git/bitbucket/growthrate_analyzes/dataFiles/28CM280814growthrate_formatted.summary', index_col=0, header=1, sep='\t')
data28CM[data28CM['Rate']<0] = 0
data28LB = pd.read_csv('/home/voolaid/git/bitbucket/growthrate_analyzes/dataFiles/28LB030914growthrate28_formatted.summary', index_col=0, header=1, sep='\t')
data37CM = pd.read_csv('/home/voolaid/git/bitbucket/growthrate_analyzes/dataFiles/37CM270814growthrate_formatted.summary', index_col=0, header=1, sep='\t')
data37LB = pd.read_csv('/home/voolaid/git/bitbucket/growthrate_analyzes/dataFiles/37LB040914growthrate_formatted.summary', index_col=0, header=1, sep='\t')

donor_list = strain_position_on_plate['donorAB'] + strain_position_on_plate['donorABminus']
recipient_list = strain_position_on_plate['recipAB'] + strain_position_on_plate['recipABminus']
transcon_list = strain_position_on_plate['transcAB'] + strain_position_on_plate['transcABminus']

donor28CM = (data28CM.ix[donor_list, 'Rate'], data28CM.ix[donor_list, 'Max OD'])
recip28CM = (data28CM.ix[recipient_list, 'Rate'], data28CM.ix[recipient_list, 'Max OD'])
transc28CM = (data28CM.ix[transcon_list, 'Rate'], data28CM.ix[transcon_list, 'Max OD'])

cm28 = (donor28CM, recip28CM, transc28CM)
cm37 = ((data37CM.ix[donor_list, 'Rate'], data37CM.ix[donor_list, 'Max OD']), (data37CM.ix[recipient_list, 'Rate'], data37CM.ix[recipient_list, 'Max OD']), (data37CM.ix[transcon_list, 'Rate'], data37CM.ix[transcon_list, 'Max OD']))
lb37 = ((data37LB.ix[donor_list, 'Rate'], data37LB.ix[donor_list, 'Max OD']), (data37LB.ix[recipient_list, 'Rate'], data37LB.ix[recipient_list, 'Max OD']), (data37LB.ix[transcon_list, 'Rate'], data37LB.ix[transcon_list, 'Max OD']))
lb28 = ((data28LB.ix[donor_list, 'Rate'], data28LB.ix[donor_list, 'Max OD']), (data28LB.ix[recipient_list, 'Rate'], data28LB.ix[recipient_list, 'Max OD']), (data28LB.ix[transcon_list, 'Rate'], data28LB.ix[transcon_list, 'Max OD']))

all_together = [cm28, cm37, lb28, lb37]

growthrates = []
max_od = []

media = ['cm28', 'cm37', 'lb28', 'lb37']
strain = ['donor', 'recip', 'transc']
names = []
for media, item in enumerate(all_together[:5]):
    for strain, item in enumerate(item):
        names.append((media, strain+5, 8)) #8 means AB+
        names.append((media, strain+5, 9)) #9 means AB-
        growthrates.append(item[0][:8])
        growthrates.append(item[0][8:])
        max_od.append(item[1][:8])
        max_od.append(item[1][:8])

#Making a library with strain names to compile in later loop
media_strain_dict = {0:'cm28_', 1:'cm37_', 2:'lb28_', 3:'lb37_', 5:'donor_', 6:'recip_', 7:'transc_', 8:'AB+', 9:'AB-' }

#objects that contain all different pairwise combinations of strains, growthrates, and maxOD. To later use it to calculate in statistic calculations
growthrate = tuple(combinations(growthrates, 2))
names_per = tuple(combinations(names, 2))
max_od = tuple(combinations(max_od, 2))

#print str(media_strain_dict[names_per[0][0][0]])+str(media_strain_dict[names_per[0][0][1]])+str(media_strain_dict[names_per[0][0][2]])
#print len(names_per), len(growthrate)

#statistics will be calculated and printed out, no file created.
i = 0
while i in range(len(growthrate)):
    first_parameter = str(media_strain_dict[names_per[i][0][0]])+str(media_strain_dict[names_per[i][0][1]])+str(media_strain_dict[names_per[i][0][2]])
    second_parameter = str(media_strain_dict[names_per[i][1][0]])+str(media_strain_dict[names_per[i][1][1]])+str(media_strain_dict[names_per[i][1][2]])
    a = stats.ttest_ind(growthrate[i][0], growthrate[i][1])
    if float(a[1]) > 0.05:#which type of results print; above or below p-value 0.05
        print first_parameter + ' vs ' + second_parameter + ' t-statistic = {}  p-value = {}'.format(*a)
        i += 1
    else:
       i += 1
       continue