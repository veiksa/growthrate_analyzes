import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import collections
from matplotlib import rcParams
#import prettyplotlib as ppl
import seaborn as sns
from scipy import stats
import brewer2mpl

#location of the summaryfiles as a list 
# input_files = ['/home/voolaid/git/bitbucket/growthrate_analyzes/dataFiles/28CM280814growthrate_formatted.summary', '/home/voolaid/git/bitbucket/growthrate_analyzes/dataFiles/28LB030914growthrate28_formatted.summary', '/home/voolaid/git/bitbucket/growthrate_analyzes/dataFiles/37CM270814growthrate_formatted.summary', '/home/voolaid/git/bitbucket/growthrate_analyzes/dataFiles/37LB040914growthrate_formatted.summary']



# def make_figures(ListOfSummaryFiles):
#     strain_position_on_plate = { 'donorAB':['B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B9'],
#                              'donorABminus':['E2', 'E3', 'E4', 'E5', 'E6', 'E7', 'E8', 'E9'], 
#                              'recipAB':['C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9'], 
#                              'recipABminus':['F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'F8', 'F9'],
#                              'transcAB':['D2', 'D3', 'D4', 'D5', 'D6', 'D7', 'D8', 'D9'],
#                              'transcABminus':['G2', 'G3', 'G4', 'G5', 'G6', 'G7', 'G8', 'G9']}
#     for summaryfile in ListOfSummaryFiles:
#         data = pd.read_csv(summaryfile, index_col=0, header=1, sep='\t')
#         #print summaryfile
#         NamesAndData = []
#         for key in strain_position_on_plate:
#             #print summaryfile
#             #print key
#             NamesAndData.append((summaryfile.split('/')[-1][0:4] + key, (data.ix[strain_position_on_plate[key], 'Rate'], data.ix[strain_position_on_plate[key], 'Max OD'])))
#             dict_namesData = collections.defaultdict(list)
#             for k, v in NamesAndData:
#                 dict_namesData[k].append(v)
#                 sorted_NamesAndData = collections.OrderedDict(sorted(dict_namesData.items()))

#         return sorted_NamesAndData['28CMdonorAB']
# print make_figures(input_files)

strain_position_on_plate = { 'donorAB':['B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B9'],
                              'donorABminus':['E2', 'E3', 'E4', 'E5', 'E6', 'E7', 'E8', 'E9'],
                             'recipAB':['C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9'], 
                              'recipABminus':['F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'F8', 'F9'],
                              'transcAB':['D2', 'D3', 'D4', 'D5', 'D6', 'D7', 'D8', 'D9'],
                              'transcABminus':['G2', 'G3', 'G4', 'G5', 'G6', 'G7', 'G8', 'G9']}


data28CM = pd.read_csv('dataFiles/28CM280814growthrate_formatted.summary', index_col=0, header=0, na_values = '----')
data28CM[data28CM['Rate']<0] = 0
data28LB = pd.read_csv('dataFiles/28LB030914growthrate28_formatted.summary', index_col=0, header=0, na_values = '----' )
data37CM = pd.read_csv('dataFiles/37CM270814growthrate_formatted.summary', index_col=0, header=0, na_values = '----')
data37LB = pd.read_csv('dataFiles/37LB040914growthrate_formatted.summary', index_col=0, header=0, na_values = '----')

donor_list = strain_position_on_plate['donorAB'] + strain_position_on_plate['donorABminus']
recipient_list = strain_position_on_plate['recipAB'] + strain_position_on_plate['recipABminus']
transcon_list = strain_position_on_plate['transcAB'] + strain_position_on_plate['transcABminus']

donor28CM = (data28CM.ix[donor_list, 'Rate'], data28CM.ix[donor_list, 'Max OD'])
recip28CM = (data28CM.ix[recipient_list, 'Rate'], data28CM.ix[recipient_list, 'Max OD'])
transc28CM = (data28CM.ix[transcon_list, 'Rate'], data28CM.ix[transcon_list, 'Max OD'])

cm28 = (donor28CM, recip28CM, transc28CM)
cm37 = ((data37CM.ix[donor_list, 'Rate'], data37CM.ix[donor_list, 'Max OD']), (data37CM.ix[recipient_list, 'Rate'], data37CM.ix[recipient_list, 'Max OD']), (data37CM.ix[transcon_list, 'Rate'], data37CM.ix[transcon_list, 'Max OD']))
lb37 = ((data37LB.ix[donor_list, 'Rate'], data37LB.ix[donor_list, 'Max OD']), (data37LB.ix[recipient_list, 'Rate'], data37LB.ix[recipient_list, 'Max OD']), (data37LB.ix[transcon_list, 'Rate'], data37LB.ix[transcon_list, 'Max OD']))
lb28 = ((data28LB.ix[donor_list, 'Rate'], data28LB.ix[donor_list, 'Max OD']), (data28LB.ix[recipient_list, 'Rate'], data28LB.ix[recipient_list, 'Max OD']), (data28LB.ix[transcon_list, 'Rate'], data28LB.ix[transcon_list, 'Max OD']))

#colorbrewer2 Dark2 qualitative color table
dark2_colors = brewer2mpl.get_map('Dark2', 'Qualitative', 7).mpl_colors

# Set up some better defaults for matplotlib

#rcParams['figure.figsize'] = (10, 6)
#rcParams['figure.dpi'] = 150
rcParams['axes.color_cycle'] = dark2_colors
rcParams['lines.linewidth'] = 2
rcParams['axes.facecolor'] = 'white'
#rcParams['font.size'] = 14
rcParams['patch.edgecolor'] = 'white'
rcParams['patch.facecolor'] = dark2_colors[0]
rcParams['font.family'] = 'StixGeneral'

rcParams['xtick.direction'] = 'out'
sns.set_style('white')

red = (0.78, 0.22, 0.18) # RGB triplet

#makes figure canvas and adds subplots
fig, axes = plt.subplots(ncols=3, nrows=2, figsize=(21.0, 13.0))
ax1, ax2, ax3, ax4, ax5, ax6 = axes.flat



bar_locations = np.arange(8)
bar_with = 0.4

first_bar_legend_box = (0.45, 1.29)
first_dot_legend_box = (1.02, 1.29)
second_bar_legend_box = (0.45, 1.29)
second_dot_legend_box = (1.02, 1.29)
third_bar_legend_box = (0.45, 1.29)
third_dot_legend_box = (1.02, 1.29)
whitespace_hight = 0.5
whitespace_between = 0.3

cm28_barlim = [0, 0.2]
cm28_dotlim = [0, 0.2]
cm37_barlim = [0, 0.2]
cm37_dotlim = [0, 0.2]

lb28_barlim = [0, 0.2]
lb28_dotlim = [0, 1.2]
lb37_barlim = [0, 0.2]
lb37_dotlim = [0, 1.3]

#========================================================================================================================#
###Figures for CM media and 28C
#barplot data for the donor CM28
ax1.bar(bar_locations, cm28[0][0][:8], bar_with, align='center', label='28CM Donor AB+')
ax1.bar(bar_locations + (bar_with), cm28[0][0][8:], bar_with, color=red, align='center', label='28CM Donor AB-')
ax1.set_ylabel('Growth Rate', fontsize='large')
ax1.set_title('Grwoth Rate and maximum OD value for Donor at 28C', y='1.03')
ax1.legend(bbox_to_anchor=first_bar_legend_box, ncol=1, fontsize='medium')
ax1.xaxis.set_ticklabels([])
ax1.set_ylim(cm28_barlim)
#plot data for the second y-axis donor CM28
ax1_sec = ax1.twinx()
ax1_sec.plot(cm28[0][1][:8],'D', color='#282926',ms=7.0, label='Max OD 28CM Donor AB+')
ax1_sec.plot(cm28[0][1][8:],'*', color='#282926',ms=8.0, label='Max OD 28CM Donor AB-')
ax1_sec.set_ylim(cm28_dotlim)
#ax1_sec.set_ylabel('Max OD')
ax1_sec.legend(bbox_to_anchor=first_dot_legend_box, ncol=1, fontsize='medium')

#barplot data for recipient CM28
plt.subplots_adjust(hspace=whitespace_hight, wspace=whitespace_between)
ax2.bar(bar_locations, cm28[1][0][:8], bar_with, align='center', label='28CM Recip AB+')
ax2.bar(bar_locations + (bar_with), cm28[1][0][8:], bar_with, color=red, align='center', label='28CM Recip AB-')
ax2.set_title('Grwoth Rate and maximum OD value for Recipient 28CM', y='1.03')
ax2.legend(bbox_to_anchor=second_bar_legend_box, ncol=1, fontsize='medium')
ax2.xaxis.set_ticklabels([])
ax2.set_ylim(cm28_barlim)

#Second y-axis data for recipient CM28
ax2_sec = ax2.twinx()
ax2_sec.plot(cm28[1][1][:8], 'D', color='#282926', ms=7.0, label='Max OD 28CM Recip AB+')
ax2_sec.plot(cm28[1][1][8:], '*', color='#282926', ms=8.0, label='Max OD 28CM Recip AB-')
ax2_sec.legend(bbox_to_anchor=second_dot_legend_box, ncol=1, fontsize='medium')
ax2_sec.set_ylim(cm28_dotlim)

#barplot data for transconjugant CM28
plt.subplots_adjust(hspace=whitespace_hight, wspace=whitespace_between)
ax3.bar(bar_locations, cm28[2][0][:8], bar_with, align='center', label='28CM Transc AB+')
ax3.bar(bar_locations + (bar_with), cm28[2][0][8:], bar_with, color=red, align='center', label='28CM Transc AB-')
ax3.set_title('Grwoth Rate and maximum OD value for Transconjugant 28CM', y='1.03')
ax3.legend(bbox_to_anchor=third_bar_legend_box, ncol=1, fontsize='medium')
ax3.xaxis.set_ticklabels([])
ax3.set_ylim(cm28_barlim)

#Second y-axis data for transconjugant CM28
ax3_sec = ax3.twinx()
ax3_sec.plot(cm28[2][1][:8], 'D', color='#282926', ms=7.0, label='Max OD 28CM Transc AB+')
ax3_sec.plot(cm28[2][1][8:], '*', color='#282926', ms=8.0, label='Max OD 28CM Transc AB-')
ax3_sec.set_ylabel('Max OD', fontsize='large')
ax3_sec.set_ylim(cm28_dotlim)
plt.legend(bbox_to_anchor=third_dot_legend_box, ncol=1, fontsize='medium')

#=======================================
###Figures for CM media and 37 degrees
#barplot data for donor CM37
ax4.bar(bar_locations, cm37[0][0][:8], bar_with, align='center', label='37CM Donor AB+')
ax4.bar(bar_locations + (bar_with), cm37[0][0][8:] , bar_with, color=red, align='center', label='37CM Donor AB-')
ax4.set_ylabel('Growth Rate', fontsize='large')
ax4.set_title('Grwoth Rate and maximum OD value for Donor at 37C', y='1.03')
ax4.legend(bbox_to_anchor=first_bar_legend_box, ncol=1, fontsize='medium')
ax4.xaxis.set_ticklabels([])
ax4.set_ylim(cm37_barlim)

#plot data for the second axis for the donor
ax4_sec = ax4.twinx()
ax4_sec.plot(cm37[0][1][:8], 'D', color='#282926', ms=7.0, label='Max OD 37CM Donor AB+')
ax4_sec.plot(cm37[0][1][8:], '*', color='#282926', ms=8.0, label='Max OD 37CM Donor AB-')
ax4_sec.set_ylim(cm37_dotlim)

#ax1_sec.set_ylabel('Max OD')
plt.legend(bbox_to_anchor=first_dot_legend_box, ncol=1, fontsize='medium')

#barplot data for recipient CM37
plt.subplots_adjust(hspace=whitespace_hight, wspace=whitespace_between)
ax5.bar(bar_locations, cm37[1][0][:8], bar_with,  align='center', label='37CM Recip AB+')
ax5.bar(bar_locations + (bar_with), cm37[1][0][8:], bar_with, color=red, align='center', label='37CM Recip AB-')
ax5.set_title('Grwoth Rate and maximum OD value for Recipient 37CM', y='1.03')
ax5.legend(bbox_to_anchor=second_bar_legend_box, ncol=1, fontsize='medium')
ax5.xaxis.set_ticklabels([])
ax5.set_ylim(cm37_barlim)

#Second y-axis data for recipient CM37
ax5_sec = ax5.twinx()
ax5_sec.plot(cm37[1][1][:8], 'D', color='#282926', ms=7.0, label='Max OD 37CM Recip AB+')
ax5_sec.plot(cm37[1][1][8:], '*', color='#282926', ms=8.0, label='Max OD 37CM Recip AB-')
ax5_sec.set_ylim(cm37_dotlim)
plt.legend(bbox_to_anchor=second_dot_legend_box, ncol=1, fontsize='medium')

#barplot data for transconjugant CM37
plt.subplots_adjust(hspace=whitespace_hight, wspace=whitespace_between)
ax6.bar(bar_locations, cm37[2][0][:8], bar_with,  align='center', label='37CM Transc AB+')
ax6.bar(bar_locations + (bar_with), cm37[2][0][8:], bar_with, color=red, align='center', label='37CM Transc AB-')
ax6.set_title('Grwoth Rate and maximum OD value for Transconjugant 37CM', y='1.03')
ax6.legend(bbox_to_anchor=third_bar_legend_box, ncol=1, fontsize='medium')
ax6.xaxis.set_ticklabels([])
ax6.set_ylim(cm37_barlim)

#Second y-axis data for recipient CM37
ax6_sec = ax6.twinx()
ax6_sec.plot(cm37[2][1][:8], 'D', color='#282926', ms=7.0, label='Max OD 37CM Transc AB+')
ax6_sec.plot(cm37[2][1][8:], '*', color='#282926', ms=8.0, label='Max OD 37CM Transc AB-')
ax6_sec.set_ylabel('Max OD', fontsize='large')
ax6_sec.set_ylim(cm37_dotlim)
plt.legend(bbox_to_anchor=third_dot_legend_box, ncol=1, fontsize='medium')

#plt.savefig('/home/voolaid/git/bitbucket/growthrate_analyzes/figures/CMgrowthratesMaxOD_MinusNeg.pdf', bbox_inches='tight')
#plt.savefig('/home/voolaid/git/bitbucket/growthrate_analyzes/figures/CMgrowthratesMaxOD_MinusNeg.png', bbox_inches='tight')

#plt.show()
#========================================================================================================================#

fig1, axes = plt.subplots(ncols=3, nrows=2, figsize=(21.0, 13.0))
ax7, ax8, ax9, ax10, ax11, ax12 = axes.flat

###Figures for LB media and 28C
#barplot data for the donor LB28
ax7.bar(bar_locations, lb28[0][0][:8], bar_with,  align='center', label='28LB Donor AB+')
ax7.bar(bar_locations + (bar_with), lb28[0][0][8:] , bar_with, color=red, align='center', label='28LB Donor AB-')
ax7.set_ylabel('Growth Rate', fontsize='large')
ax7.set_title('Grwoth Rate and maximum OD value for Donor at 28C', y='1.03')
ax7.legend(bbox_to_anchor=first_bar_legend_box, ncol=1, fontsize='medium')
ax7.xaxis.set_ticklabels([])
ax7.set_ylim(lb28_barlim)
#plot data for the second y-axis donor LB28
ax7_sec = ax7.twinx()
ax7_sec.plot(lb28[0][1][:8], 'D', color='#282926', ms=7.0, label='Max OD 28LB Donor AB+')
ax7_sec.plot(lb28[0][1][8:], '*', color='#282926', ms=8.0, label='Max OD 28LB Donor AB-')
ax7_sec.set_ylim(lb28_dotlim)
#ax7_sec.set_ylabel('Max OD')
plt.legend(bbox_to_anchor=first_dot_legend_box, ncol=1, fontsize='medium')

#barplot data for recipient LB28
plt.subplots_adjust(hspace=whitespace_hight, wspace=whitespace_between)
ax8.bar(bar_locations, lb28[1][0][:8], bar_with,  align='center', label='28LB Recip AB+')
ax8.bar(bar_locations + (bar_with), lb28[1][0][8:], bar_with, color=red, align='center', label='28LB Recip AB-')
ax8.set_title('Grwoth Rate and maximum OD value for Recipient 28LB', y='1.03')
ax8.legend(bbox_to_anchor=second_bar_legend_box, ncol=1, fontsize='medium')
ax8.xaxis.set_ticklabels([])
ax8.set_ylim(lb28_barlim)

#Second y-axis data for recipient LB28
ax8_sec = ax8.twinx()
ax8_sec.plot(lb28[1][1][:8], 'D', color='#282926', ms=7.0, label='Max OD 28LB Recip AB+')
ax8_sec.plot(lb28[1][1][8:], '*', color='#282926', ms=8.0, label='Max OD 28LB Recip AB-')
ax8_sec.set_ylim(lb28_dotlim)
plt.legend(bbox_to_anchor=second_dot_legend_box, ncol=1, fontsize='medium')

#barplot data for transconjugant LB28
plt.subplots_adjust(hspace=whitespace_hight, wspace=whitespace_between)
ax9.bar(bar_locations, lb28[2][0][:8], bar_with,  align='center', label='28LB Transc AB+')
ax9.bar(bar_locations + (bar_with), lb28[2][0][8:], bar_with, color=red, align='center', label='28LB Transc AB-')
ax9.set_title('Grwoth Rate and maximum OD value for Transconjugant 28LB', y='1.03')
ax9.legend(bbox_to_anchor=third_bar_legend_box, ncol=1, fontsize='medium')
ax9.xaxis.set_ticklabels([])
ax9.set_ylim(lb28_barlim)

#Second y-axis data for transconjugant LB28
ax9_sec = ax9.twinx()
ax9_sec.plot(lb28[2][1][:8], 'D', color='#282926', ms=7.0, label='Max OD 28LB Transc AB+')
ax9_sec.plot(lb28[2][1][8:], '*', color='#282926', ms=8.0, label='Max OD 28LB Transc AB-')
ax9_sec.set_ylabel('Max OD', fontsize='large')
ax9_sec.set_ylim(lb28_dotlim)
plt.legend(bbox_to_anchor=third_dot_legend_box, ncol=1, fontsize='medium')

#=======================================
###Figures for LB media and 37 degrees
#barplot data for donor LB37
ax10.bar(bar_locations, lb37[0][0][:8], bar_with,  align='center', label='37LB Donor AB+')
ax10.bar(bar_locations + (bar_with), lb37[0][0][8:] , bar_with, color=red, align='center', label='37LB Donor AB-')
ax10.set_ylabel('Growth Rate', fontsize='large')
ax10.set_title('Grwoth Rate and maximum OD value for Donor at 37C', y='1.03')
ax10.legend(bbox_to_anchor=first_bar_legend_box, ncol=1, fontsize='medium')
ax10.xaxis.set_ticklabels([])
ax10.set_ylim(lb37_barlim)

#plot data for the second axis for the donor
ax10_sec = ax10.twinx()
ax10_sec.plot(lb37[0][1][:8], 'D', color='#282926', ms=7.0, label='Max OD 37LB Donor AB+')
ax10_sec.plot(lb37[0][1][8:], '*', color='#282926', ms=8.0, label='Max OD 37LB Donor AB-')
ax10_sec.set_ylim(lb37_dotlim)

#ax1_sec.set_ylabel('Max OD')
plt.legend(bbox_to_anchor=first_dot_legend_box, ncol=1, fontsize='medium')

#barplot data for recipient LB37
plt.subplots_adjust(hspace=whitespace_hight, wspace=whitespace_between)
ax11.bar(bar_locations, lb37[1][0][:8], bar_with,  align='center', label='37LB Recip AB+')
ax11.bar(bar_locations + (bar_with), lb37[1][0][8:], bar_with, color=red, align='center', label='37LB Recip AB-')
ax11.set_title('Grwoth Rate and maximum OD value for Recipient 37LB', y='1.03')
ax11.legend(bbox_to_anchor=second_bar_legend_box, ncol=1, fontsize='medium')
ax11.xaxis.set_ticklabels([])
ax11.set_ylim(lb37_barlim)

#Second y-axis data for recipient LB37
ax11_sec = ax11.twinx()
ax11_sec.plot(lb37[1][1][:8], 'D', color='#282926', ms=7.0, label='Max OD 37LB Recip AB+')
ax11_sec.plot(lb37[1][1][8:], '*', color='#282926', ms=8.0, label='Max OD 37LB Recip AB-')
plt.legend(bbox_to_anchor=second_dot_legend_box, ncol=1, fontsize='medium')
ax11_sec.set_ylim(lb37_dotlim)

#barplot data for transconjugant LB37
plt.subplots_adjust(hspace=whitespace_hight, wspace=whitespace_between)
ax12.bar(bar_locations, lb37[2][0][:8], bar_with,  align='center', label='37LB Transc AB+')
ax12.bar(bar_locations + (bar_with), lb37[2][0][8:], bar_with, color=red, align='center', label='37LB Transc AB-')
ax12.set_title('Grwoth Rate and maximum OD value for Transconjugant 37LB', y='1.03')
ax12.legend(bbox_to_anchor=third_bar_legend_box, ncol=1, fontsize='medium')
ax12.xaxis.set_ticklabels([])
ax12.set_ylim(lb37_barlim)

#Second y-axis data for recipient LB37
ax12_sec = ax12.twinx()
ax12_sec.plot(lb37[2][1][:8], 'D', color='#282926', ms=7.0, label='Max OD 37LB Transc AB+')
ax12_sec.plot(lb37[2][1][8:], '*', color='#282926', ms=8.0, label='Max OD 37LB Transc AB-')
ax12_sec.set_ylabel('Max OD', fontsize='large')
plt.legend(bbox_to_anchor=third_dot_legend_box, ncol=1, fontsize='medium')
ax12_sec.set_ylim(lb37_dotlim)


#plt.savefig('/home/voolaid/git/bitbucket/growthrate_analyzes/figures/LBgrowthratesMaxOD_MinusNeg.pdf', bbox_inches='tight')
#plt.savefig('/home/voolaid/git/bitbucket/growthrate_analyzes/figures/LBgrowthratesMaxOD_MinusNeg.png', bbox_inches='tight')

plt.show()
